#!/bin/bash

echo "Updating apt packages..."
sudo apt update && \
    sudo apt upgrade -y && \
    sudo apt autoremove -y

echo "Installing useful apt packages..."
sudo apt install -y \
    wget \
    curl \
    git \
    zsh \
    vim \
    htop \
    tmux \
    powerline \
    ripgrep \
    dconf-cli \
    gnome-tweaks


echo "Downloading and installing Oh-My-Zsh..."
wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O - | zsh

echo "Setting ZSH as default shell..."
chsh -s $(which zsh) $(whoami)

echo "Downloading and installing fonts..."
mkdir -p /tmp/fonts
wget https://github.com/powerline/powerline/raw/develop/font/PowerlineSymbols.otf -O /tmp/fonts/PowerlineSymbols.otf
git clone https://github.com/gabrielelana/awesome-terminal-fonts.git /tmp/awesome-terminal-fonts

mkdir -p ~/.fonts
cp /tmp/fonts/PowerlineSymbols.otf ~/.fonts/
cp -f /tmp/awesome-terminal-fonts/fonts/*.ttf ~/.fonts
cp -f /tmp/awesome-terminal-fonts/fonts/*.sh ~/.fonts
git --git-dir=/tmp/awesome-terminal-fonts/.git --work-tree=/tmp/awesome-terminal-fonts checkout patching-strategy
cp -f /tmp/awesome-terminal-fonts/patched/*.ttf ~/.fonts
cp -f /tmp/awesome-terminal-fonts/patched/*.sh ~/.fonts
wget -O ~/.fonts/Dejavu_Mono_NF.ttf https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/DejaVuSansMono/Regular/complete/DejaVu%20Sans%20Mono%20Nerd%20Font%20Complete.ttf?raw=true
wget -O ~/.fonts/Droid_Mono_NF.otf https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/DroidSansMono/complete/Droid%20Sans%20Mono%20Nerd%20Font%20Complete.otf\?raw\=true


mkdir -p ~/.config/fontconfig/conf.d
cp 10-symbols.conf ~/.config/fontconfig/conf.d/
fc-cache -vf ~/.fonts/

echo "Downloading and installing powerlevel10k..."
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/themes/powerlevel10k

echo "Downloading and installing vim plugins..."
mkdir -p ~/.vim/autoload ~/.vim/bundle
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

git clone https://github.com/vim-airline/vim-airline ~/.vim/bundle/vim-airline
git clone https://github.com/vim-airline/vim-airline-themes ~/.vim/bundle/vim-airline-themes
git clone https://github.com/nvie/vim-flake8.git ~/.vim/bundle/vim-flake8
git clone --recursive https://github.com/davidhalter/jedi-vim.git ~/.vim/bundle/jedi-vim

echo "Downloading and installing zsh plugins..."
git clone https://github.com/z-shell/H-S-MW.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/H-S-MW
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

echo "Downloading and installing tmux plugins..."
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
git clone https://github.com/jimeh/tmux-themepack.git ~/.tmux-themepack

cp -r .p10k.zsh .tmux.conf .zshrc .vimrc ~/

echo ''
echo '---------------------------------------------------------------'
echo 'System fonts have been installed. However, to use them you'
echo 'will need to configure your terminal to use the appropriate'
echo 'font. I have been using'
echo '"Droid Sans Mono Awesome Regular" at size 11.'
echo "Go to Terminal's Preferences/Profile and set the"
echo 'Custom Font to use system default. Then, go to gnome-tweaks'
echo 'and configure your correct font.'
echo '---------------------------------------------------------------'
echo ''
echo '---------------------------------------------------------------'
echo 'System is now configured. You must reboot in order for some'
echo 'terminal settings (such as login shell) to properly take'
echo 'effect. Please do so now.'
echo '---------------------------------------------------------------'
echo ''
