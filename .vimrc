execute pathogen#infect()
syntax on
filetype plugin indent on

let linelengthlimit = 95

let upper_warning_length = linelengthlimit + 1
let lower_warning_length = linelengthlimit - 1

let filemask = "*.c,*.cc,*.h,*.cs,*.py,*.sh,*.zsh"

let warning_column = 0
let early_length_warning = 0
let long_line_warning = 1
let mark_extra_whitespace = 1
let tabsize = 4

execute "set tabstop=".tabsize

execute "set shiftwidth=".tabsize

execute "set softtabstop=".tabsize

set expandtab
set smartindent
set autoindent

set background=dark
"colorscheme solarized
hi Normal guibg=NONE ctermbg=NONE

let g:airline_powerline_fonts = 1
let g:airline_theme='powerlineish'

if mark_extra_whitespace
    highlight ExtraWhitespace ctermbg=red guibg=red
    match ExtraWhitespace /\s\+$/
    autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
    autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
    autocmd InsertLeave * match ExtraWhitespace /\s\+$/
    autocmd BufWinLeave * call clearmatches()
endif
if warning_column
    " Add a marker at the first over-length character.
    execute 'set colorcolumn=' . upper_warning_length
    highlight ColorColumn ctermbg=0 guibg=lightgrey
endif

" Make the vertical split bar much nicer
autocmd ColorScheme * highlight VertSplit cterm=NONE ctermfg=Green ctermbg=NONE
set encoding=utf8
set fillchars+=vert:│

if early_length_warning
    " Highlight text that is close to the character line limit.
    execute "autocmd BufWinEnter " .
    \           filemask .
    \           " let w:m1=matchadd('Search', '\\%<" .
    \           upper_warning_length .
    \           "v.\\%>" .
    \           lower_warning_length .
    \           "v', -1)"
endif

if long_line_warning
    " Highlight text that is over 80 characters long using the ErrorMsg
    "highlight rule.
    execute "autocmd BufWinEnter " .
    \           filemask .
    \           " let w:m2=matchadd('ErrorMsg', '\\%>" .
    \           linelengthlimit .
    \           "v.\\+', -1)"
endif

" Prevent vim from automatically wrapping long lines. This tends to break things
" like python scripts.
set textwidth=0
set wrapmargin=0

" Leave 2 spaces on the status line, for powerline stuff
set laststatus=2

" Enable 256 color mode
set t_Co=256

" Enable mouse mode, for full mouse control.
set mouse=a
" Let the mouse work correctly past te 220th column.
set ttymouse=sgr

" Set the default open locations of new splits to below and to the right.
set splitbelow
set splitright
"
" Disables equal splitting when performing splits. This makes it so that a
" split will only split the current window and not resize the others. Also
" when closing a split (such as a preview window) the other splits will be
" unaffected by it.
set noequalalways

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

" Fix the tmux/vim incompatibility with ctrl-arrows
if &term =~ '^screen'
    " tmux will send xterm-style keys when its xterm-keys option is on
    execute "set <xUp>=\e[1;*A"
    execute "set <xDown>=\e[1;*B"
    execute "set <xRight>=\e[1;*C"
    execute "set <xLeft>=\e[1;*D"
endif
